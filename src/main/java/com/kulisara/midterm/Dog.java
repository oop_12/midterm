package com.kulisara.midterm;

import com.kulisara.ClassDog;

public class Dog {
    public static void main(String[] args) {
        ClassDog Dog1 = new ClassDog();
        ClassDog Dog2 = new ClassDog();
        ClassDog Dog3 = new ClassDog();
        Dog1.setName("Dalmatian");
        Dog1.setHP(150);
        Dog2.setName("Husky");
        Dog2.setHP(350);
        Dog3.setName("German Shepherd");
        Dog3.setHP(400);

        Dog1.print();
        System.out.println();
        Dog2.print();
        System.out.println();
        Dog3.print();



    }

}
