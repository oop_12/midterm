package com.kulisara.midterm;

public class ClassCharacter {
    private String Character;
    private int HP;
    private int Agility;
  
    private int Magic;
    private int Intellect;
    private int defense;

    public void setCharacter(String Character) {
        this.Character = Character;
    }

    public String getCharacter() {
        return Character;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getHP() {
        return HP;

    }

    public void setIntellect(int Intellect) {
        this.Intellect = Intellect;
    }

    public int getIntellect() {
        return Intellect;
    }
    

    public int Agility() {
        Agility = defense +200;
        return Agility;
    }

    public int Magic() {
        Magic = Intellect + 500;
        return Magic;
    }

    public int defense() {
        defense = HP + 1000;
        return defense;
    }

    public void print() {
        System.out.println(" Character : " + Character );
        System.out.println(" HP : " + HP);
        System.out.println(" Intellect: " + Intellect);
        System.out.println(" Agility: " + Agility());
        System.out.println(" Magic : " + Magic());
        System.out.println(" defense : " + defense());

    }

}
