package com.kulisara.midterm;

public class Character {
    public static void main(String[] args) {
        ClassCharacter Character1 = new ClassCharacter();
        ClassCharacter Character2 = new ClassCharacter();
        ClassCharacter Character3 = new ClassCharacter();
        Character1.setCharacter("Witch");
        Character1.setHP(250);
        Character1.setIntellect(2000);
    

        Character2.setCharacter("Swordsman");
        Character2.setHP(2000);
        Character2.setIntellect(0);


        Character3.setCharacter("Scientist");
        Character3.setHP(500);
        Character3.setIntellect(750);


        Character1.print();
        System.out.println();
        Character2.print();
        System.out.println();
        Character3.print();



    }

}
