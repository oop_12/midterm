package com.kulisara;

public class ClassDog {
    private String name;
    private int HP;
    private int defense;
    private int ATK;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getHP() {
        return HP;
    }

    public int defense() {
        defense = HP + 100;
        return defense;
    }
    public int ATK() {
        ATK = HP + 200;
        return ATK;
    }

    public void print() {
        System.out.println("Name : " + name );
        System.out.println( " HP : " + HP);
        System.out.println(" defense : " + defense());
        System.out.println(" ATK : " + ATK());


    }
 
}
